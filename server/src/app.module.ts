import { Module } from "@nestjs/common";
import { ConfigModule, ConfigService } from "@nestjs/config";
import jwtConfig from "./config/jwt.config";
import {
  graphqlConfig,
  loggerConfig,
  sequelizeConfig,
  SyncTablesService,
} from "./config";
import { SequelizeModule } from "@nestjs/sequelize";
import { GraphQLFederationModule } from "@nestjs/graphql";
import GraphQLJSON from "graphql-type-json";
import { GraphQLDateTime } from "graphql-iso-date";
import { WinstonModule } from "nest-winston";

import { User } from "./models";
import { AuthModule } from "./auth/auth.module";

@Module({
  providers: [SyncTablesService],
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [sequelizeConfig, graphqlConfig, jwtConfig, loggerConfig],
    }),
    WinstonModule.forRootAsync({
      useFactory: (config: ConfigService) => config.get("logger"),
      inject: [ConfigService],
    }),
    SequelizeModule.forRootAsync({
      useFactory: (config: ConfigService) => ({
        ...config.get("sequelize"),
        models: [User],
      }),
      inject: [ConfigService],
    }),
    GraphQLFederationModule.forRootAsync({
      useFactory: (config: ConfigService) => ({
        ...config.get("graphql"),
        typePaths: ["./**/*.graphql"],
        installSubscriptionHandlers: false,
        resolvers: [{ JSON: GraphQLJSON }, { DateTime: GraphQLDateTime }],
      }),
      inject: [ConfigService],
    }),

    AuthModule,
  ],
})
export class AppModule {}
