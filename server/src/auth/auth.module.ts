import { Module } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtModule, JwtModuleOptions } from "@nestjs/jwt";
import { PassportModule } from "@nestjs/passport";
import { JwtStrategy } from "./jwt.strategy";
import { AuthResolver } from "./resolvers/auth.resolver";

@Module({
  imports: [
    PassportModule.register({ defaultStrategy: "jwt" }),
    JwtModule.registerAsync({
      useFactory: (config: ConfigService) =>
        config.get<JwtModuleOptions>("jwt"),
      inject: [ConfigService],
    }),
  ],
  providers: [AuthResolver, JwtStrategy],
})
export class AuthModule {}
