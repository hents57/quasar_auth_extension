import { Injectable } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { JwtModuleOptions } from "@nestjs/jwt";
import { PassportStrategy } from "@nestjs/passport";
import { Request } from "express";
import { Strategy } from "passport-jwt";

const headerExtractor = (req: Request): string | null => {
  if (req && req.headers && req.headers.authorization) {
    return req.headers.authorization.replace("Bearer ", "");
  }

  return null;
};

@Injectable()
export class JwtStrategy extends PassportStrategy(Strategy) {
  constructor(private readonly configService: ConfigService) {
    super({
      jwtFromRequest: headerExtractor,
      secretOrKey: configService.get<JwtModuleOptions>("jwt").secret,
    });
  }
}
