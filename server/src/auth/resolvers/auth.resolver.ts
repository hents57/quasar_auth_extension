import { Inject } from "@nestjs/common";
import { ConfigService } from "@nestjs/config";
import { Args, Mutation, Resolver } from "@nestjs/graphql";
import { JwtService } from "@nestjs/jwt";
import { ApolloError } from "apollo-server";
import * as bcryptjs from "bcryptjs";
import { WINSTON_MODULE_PROVIDER } from "nest-winston";
import { Logger } from "winston";
import { User } from "../../models";

@Resolver("Auth")
export class AuthResolver {
  constructor(
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    @Inject(WINSTON_MODULE_PROVIDER) private readonly logger: Logger
  ) {}

  @Mutation()
  async login(
    @Args("email") email: string,
    @Args("password") password: string
  ) {
    this.logger.info(`Login user with email ${email}`);

    const user = await User.findOne({ where: { email } });
    if (!user) {
      this.logger.error(` User not found ${email}`);
      throw new ApolloError(
        `Il n'y a pas encore de compte créé pour l'email :  ${email}`,
        "ACCOUNT_NOT_EXIST"
      );
    }

    const valid = await bcryptjs.compare(password, user.passwordHash);
    if (!valid) {
      this.logger.error(
        ` User password invalid email='${email}',password='${password}'`
      );
      return new ApolloError(
        "Mot de passe ou email invalide",
        "PASSWORD_INVALID"
      );
    }

    const accessToken = this.jwtService.sign({ sub: user.id });

    this.logger.info(
      ` User logged in email='${email}',accessToken='${accessToken}'`
    );

    return { accessToken, user };
  }
}
