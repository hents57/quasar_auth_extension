import {
  Model,
  Table,
  Column,
  CreatedAt,
  UpdatedAt,
  DataType,
  ForeignKey,
} from "sequelize-typescript";

@Table({ tableName: "User" })
export class User extends Model<User, Partial<User>> {
  @Column({ primaryKey: true, type: DataType.STRING(25) })
  id: string;

  @Column({ type: DataType.STRING(255) })
  email: string;

  @Column({ field: "password_hash", type: DataType.STRING(255) })
  passwordHash: string;
}
