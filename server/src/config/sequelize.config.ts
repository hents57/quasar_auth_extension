import { registerAs } from '@nestjs/config';
import { SequelizeOptions } from 'sequelize-typescript';


export default registerAs('sequelize', (): SequelizeOptions & { schema?: string } => {
  return {
    dialect: 'postgres',
    host: process.env.SEQUELIZE_HOST || 'localhost',
    port: parseInt(process.env.SEQUELIZE_PORT || process.env.POSTGRES_PORT || `5432`, 10),
    username: process.env.SEQUELIZE_USERNAME || process.env.POSTGRES_USER,
    password: process.env.SEQUELIZE_PASSWORD || process.env.POSTGRES_PASSWORD,
    database: process.env.SEQUELIZE_DATABASE || process.env.POSTGRES_DB,
    schema: process.env.SEQUELIZE_SCHEMA,
    sync: {
      force: false,
      alter:true
    },
  };
});
