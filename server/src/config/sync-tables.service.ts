import { Injectable } from "@nestjs/common";
import { Sequelize } from "sequelize-typescript";
import { User } from "../models";
import * as bcryptjs from "bcryptjs";
import { generateId } from "./generate-id";
@Injectable()
export class SyncTablesService {
  constructor(private sequelize: Sequelize) {
    Object.values(sequelize.models).forEach((model) => {
      Promise.all(
        Object.values(model.associations).map((association) => {
          return association.target.sync({ force: false });
        })
      );
      model.sync({ force: false });
    });
    this.load();
  }

  async load() {
    const defaultUser = await User.findOne({
      where: { email: "quasar@test.com" },
    });
    if (!defaultUser) {
      const passwordHash = await bcryptjs.hash("test1234", 12);
      await new User({
        id: generateId(),
        email: "quasar@test.com",
        passwordHash,
      }).save();
    }
  }
}
