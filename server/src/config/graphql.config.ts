import { registerAs } from '@nestjs/config';

export default registerAs('graphql', () => {
  return {
    debug: 'true' === process.env.GRAPHQL_DEBUG,
    playground: 'true' === process.env.GRAPHQL_PLAYGROUND,
  };
});
