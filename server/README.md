## Description

Port : 4000

## Installation

```bash
$ copy .env.exemple into .env
$ yarn

```

## Running the app

```bash
# development
$ yarn start

# watch mode
$ yarn dev
```

## Playground

```bash
# open in browser
$ http://localhost:4000/graphql
```
