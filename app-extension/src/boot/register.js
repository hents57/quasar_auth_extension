import AuthExtensionComponent from "../component/AuthExtensionComponent.vue";

export default ({ app }) => {
  // we globally register our component in the app
  app.component("auth-extension-component", AuthExtensionComponent);
};
