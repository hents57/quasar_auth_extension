# AuthApp (client)

Auth app extension test

## Install the dependencies into client, server and app-extension

```bash
    yarn install
```

## Install environment variables in root folder, and server

```bash
    copy .env.exemple into .env
```

## Run database container

```bash
    docker-compose up -d --build
```

## Initailise User table in server folder

```bash
    yarn start
```

## Initailise default user in server folder

```bash
    yarn start
```

## Launch client

```bash
    yarn dev
```

### Default email : quasar@test.com

### Default password: test1234
